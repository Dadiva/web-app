/**
 * Created by sergio on 4/20/17.
 */
import React, {Component} from  'react';
import ActionList from './ActionList'
import uuidV4 from 'uuid/v4';
const ActionCreator = {
  login: function (payload) {
    return {
      type: ActionList.LOGIN_USER,
      ...payload
    }
  },
  addUser: function(payload) {
    return {
      type: ActionList.ADD_USER,
      id: uuidV4(),
      ...payload
    }
  },
  logout: function(){
    return {
      type: ActionList.LOGOUT
    }
  },
  addFriend: function(payload) {
    return {
      type: ActionList.ADD_FRIEND,
      ...payload
    }
  },
  register: function (payload) {
    const id = uuidV4();
    console.log("register");
    const newPayload = {
      id: id,
      ...payload
    }
    return dispatch => {
      dispatch(this.addUser(newPayload));
      dispatch(this.login(newPayload));
    }
  },
  registerList: function (payload) {
    const id = uuidV4();
    console.log("Register list from action creator", payload);
    const newPayload = {
      id,
      ...payload,
    }
    return dispatch => {
      dispatch(this.addList(newPayload));
      //dispatch(this.enterList(newPayload));
      if (newPayload.owner_id)  dispatch(this.setListOwner(newPayload));
    }
  },
  setListOwner: function (payload) {
    console.log('Set list owner from action creator', payload)
    return {
      type: ActionList.SET_LIST_OWNER,
      ...payload
    }
  },
  addList: function (payload) {
    console.log("Add list from action creator", payload)
    const id = uuidV4();
    const newPayload = {
      id,
      ...payload
    }
    console.log("After", newPayload)
    return {
      type: ActionList.ADD_LIST,
      ...newPayload
    }
  },
  enterList: function (payload) {
    return  {
      type: ActionList.ENTER_TO_LIST,
      ...payload
    }
  },
  registerItemToList: function(payload){
    const  id = uuidV4();
    const newPayload = {
      id,
      ...payload
    }
    return dispatch => {
      dispatch(this.addItemToList(newPayload));
      dispatch(this.enterToListItem(newPayload));
    }
  },
  addItemToList: function (payload) {
    const id = uuidV4();
    const newPayload = {
      id,
      ...payload
    }
    return {
      type: ActionList.ADD_ITEM_TO_LIST,
      ...newPayload
    }
  },
  enterToListItem: function(payload) {
    return {
      type: ActionList.ENTER_TO_LIST_ITEM,
      ...payload
    }
  }
}

export default ActionCreator