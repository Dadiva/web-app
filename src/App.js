import React, { Component } from 'react';
import Main from './Components/Main'
import {HashRouter, Route, BrowserRouter as Router} from 'react-router-dom'
import {Provider} from 'react-redux';

const store = require('./store/configureStore').configure();
import ActionCreator from './Actions/ActionCreator'

store.subscribe(() => {
  const state = store.getState();
  console.log('New state', state);
})

const dummy_user = {
  name: 'Pepe',
  last_name: 'López',
  email: 'pepe@gmail.com',
  id: '1',
  password:'1234'
}

const dummy_user1 = {
  name: 'Ramón',
  last_name: 'Velarde',
  email: 'ramon@gmail.com',
  password:'1234',
  id: '2'
};
const dummy_user2 = {
  name: 'Beto',
  last_name: 'Jurado',
  email: 'beto1@gmail.com',
  password:'1234',
  id: '3'
}
const dummy_user3 = {
  name: 'Beto',
  last_name: 'Jurado',
  email: 'beto2@gmail.com',
  password:'1234',
  id: '4'
}
const dummy_user4 = {
  name: 'Beto',
  last_name: 'Jurado',
  email: 'beto3@gmail.com',
  password:'1234',
  id:'5'
}
const dummy_user5 = {
  name: 'Beto',
  last_name: 'Jurado',
  email: 'beto4@gmail.com',
  password:'1234',
  id:'6'
}
const dummy_user6 = {
  name: 'Beto',
  last_name: 'Jurado',
  email: 'beto5@gmail.com',
  password:'1234',
  id:'7'
}
 store.dispatch(ActionCreator.addUser(dummy_user));
 store.dispatch(ActionCreator.register(dummy_user1));
 store.dispatch(ActionCreator.addUser(dummy_user2));
store.dispatch(ActionCreator.addUser(dummy_user3));
store.dispatch(ActionCreator.addUser(dummy_user4));
store.dispatch(ActionCreator.addUser(dummy_user5));
store.dispatch(ActionCreator.addUser(dummy_user6));
 store.dispatch(ActionCreator.registerList({name:"Dummy List", owner_id:'1'}))
 store.dispatch(ActionCreator.registerList({name:"Dummy List1", owner_id:'1'}))
 store.dispatch(ActionCreator.registerList({name:"Dummy List2", owner_id:'2'}))
store.dispatch(ActionCreator.addList({name:"Dummy List2"}))
store.dispatch(ActionCreator.addList({name:"Dummy List2"}))
store.dispatch(ActionCreator.addList({name:"Dummy List2"}))
store.dispatch(ActionCreator.addList({name:"Dummy List2"}))
store.dispatch(ActionCreator.addFriend({friend_email: "pepe@gmail.com", owner_index:"1"}))
store.dispatch(ActionCreator.addFriend({friend_email: "beto1@gmail.com", owner_index:"1"}))
store.dispatch(ActionCreator.addFriend({friend_email: "beto2@gmail.com", owner_index:"1"}))
store.dispatch(ActionCreator.addFriend({friend_email: "beto3@gmail.com", owner_index:"1"}))
store.dispatch(ActionCreator.addFriend({friend_email: "beto4@gmail.com", owner_index:"1"}))
store.dispatch(ActionCreator.addFriend({friend_email: "beto5@gmail.com", owner_index:"1"}))
//store.dispatch(ActionCreator.logout());

class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <Router>
          <Route path="/" component={Main}/>
      </Router>
      </Provider>
    )
  }
}

export default App;
