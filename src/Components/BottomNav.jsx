/**
 * Created by sergio on 4/20/17.
 */
import React, {Component} from  'react';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';

import SocialGroup from 'material-ui/svg-icons/social/group'
import ActionHome from 'material-ui/svg-icons/action/home'
import ActionList from 'material-ui/svg-icons/action/list'

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
const socialGroup = <SocialGroup/>;
const actionList = <ActionList/>;
const actionHome = <ActionHome/>;


class BottomNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 1,
    }
  }

  select = (index) => {
    console.log('navbar index', index);
    console.log('navbar prev state',this.state.selectedIndex);
    this.setState({selectedIndex: index})
    let url = '/home/'
    switch (index) {
      case 0:
        url += 'lists'
        break;
      case 1:
        url += 'friends'
        break;
      default:
        console.log("Should never appear");
    }
    console.log('navbar post state',this.state.selectedIndex);
    //console.log(url);
    console.log(this.props.history.push(url));
  }

  render() {
    return (
          <div className="bottom-nav">
            <Paper zDepth={1}>
              <BottomNavigation selectedIndex={this.state.selectedIndex}>
                <BottomNavigationItem
                  label="Lists"
                  icon={actionList}
                  onTouchTap={() => this.select(0)}
                />
                {/*<BottomNavigationItem
                  label="Home"
                  icon={actionHome}
                  onTouchTap={() => this.select(1)}
                />*/}
                <BottomNavigationItem
                  label="Friends"
                  icon={socialGroup}
                  onTouchTap={() => this.select(1)}
                />
              </BottomNavigation>
            </Paper>
        </div>
    )
  }
}

export default BottomNav