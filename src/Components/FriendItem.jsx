/**
 * Created by sergio on 4/20/17.
 */
import React, {Component} from  'react';
import RaisedButton from 'material-ui/RaisedButton';

class FriendItem extends Component {
  render(){
    return(
      <div>Friend Item
      <RaisedButton label="Back"
                      className=""
                      onTouchTap={() => this.props.history.goBack()}
        />
      </div>
    )
  }
}
export default FriendItem