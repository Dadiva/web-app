/**
 * Created by sergio on 4/20/17.
 */
import React, {Component} from  'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import {connect} from 'react-redux';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

//For adding friends
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField';

import SocialPerson from 'material-ui/svg-icons/social/person'
const socialPerson = <SocialPerson/>;

//For actions
import ActionCreator from '../Actions/ActionCreator'
import FloatingActionButton from "material-ui/FloatingActionButton";
import ContentAdd from "material-ui/svg-icons/content/add";
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back'

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    //height: 450,
    overflowY: 'auto',
  },
};

class Friends extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAdd: false,
      emailAdd: "",
    }
  }

  handleTap(id) {
    console.log(id);
    this.props.history.push('/home/friends/friend:' + id);
  }

  handleOpen() {
    console.log("Open friends dialog from", this.props);
    this.setState({
      openAdd: true
    })
  }

  handleSubmit() {
    if (this.state.emailAdd.length > 1) {
      this.props.dispatch(ActionCreator.addFriend({
        owner_index: this.props.current_user_index,
        friend_email: this.state.emailAdd
      }));
      this.setState({
        emailAdd: "",
      })
    }
    this.handleClose();
  }

  handleClose() {
    this.setState({
      openAdd: false,
    })
    console.log("Something", this.props.users[this.props.current_user_index].friends_id);
    this.props.users[this.props.current_user_index].friends_id.map((friend) => {
      console.log("A friend", friend);
    })
  }

  renderFriends() {
    if (this.props.users[this.props.current_user_index].friends_id.length > 0) {

    }
  }

  getFriendName(tile){
    const friend = this.props.users.find((friend) => friend.id === tile)
    return friend.name + " " + friend.last_name
  }

  componentDidMount() {
    console.log("Friends", this.props)
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose.bind(this)}
      />,
      <FlatButton
        label="Ok"
        primary={true}
        onTouchTap={this.handleSubmit.bind(this)}
      >
      </FlatButton>
    ];
    console.log("Outside", this.props)
    return (
      <div className="list-main-wrapper">
        <div className="list-wrapper">
          <Dialog
            title="Add a friend to your list"
            open={this.state.openAdd}
            onRequestClose={this.handleClose.bind(this)}
            actions={actions}
            modal={false}
          >
            Enter your friend's email address
            <TextField hintText="Title"
              //className="login-item"
                       value={this.state.emailAdd}
                       onChange={(e) => {
                         this.setState({
                           emailAdd: e.target.value
                         })
                       }}
            />
          </Dialog>
          <GridList
            cellHeight={180}
            className="list"
          >
            {this.props.users[this.props.current_user_index].friends_id.map((tile) => (
              <GridTile
                key={tile}
                title={this.getFriendName(tile)}
                //actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
                onTouchTap={() => this.handleTap(tile)}
              >
                <img src={tile.img}/>
                <SocialPerson className="gift-icon"/>
              </GridTile>
            ))}
          </GridList>
        </div>
        <div className="list-buttons">
            <FloatingActionButton
              className=""
              onTouchTap={() => this.props.history.goBack()}
            >
              <NavigationArrowBack/>
            </FloatingActionButton>
            <FloatingActionButton
              className=""
              onTouchTap={this.handleOpen.bind(this)}
            >
              <ContentAdd/>
            </FloatingActionButton>
          </div>
      </div>
    )
  }
}
export default connect((state) => {
  return {
    state: state,
    users: state.users.db,
    owner_id: state.session.active_user_id,
    current_user_index: state.users.db.map((user) => {
      return user.id
    }).indexOf(state.session.active_user_id),
    current_user_object: state.users.db.find((user) => user.id === state.session.active_user_id)
  }
})(Friends)
