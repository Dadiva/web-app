/**
 * Created by sergio on 4/20/17.
 */
import React, {Component} from  'react';
import {GridList, GridTile} from 'material-ui/GridList';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import ActionGiftCard from 'material-ui/svg-icons/action/card-giftcard'
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton'
import {connect} from 'react-redux'
import ActionCreator from '../Actions/ActionCreator'


import SocialPerson from 'material-ui/svg-icons/social/person'

const actionGiftCard = <ActionGiftCard/>;

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    //height: 450,
    overflowY: 'auto',
  },
};

const tilesData = [
  {
    img: 'images/grid-list/burger-827309_640.jpg',
    title: 'Tasty burger',
    author: 'pashminu',
  },
  {
    img: 'images/grid-list/camera-813814_640.jpg',
    title: 'Camera',
    author: 'Danson67',
  },
  {
    img: 'images/grid-list/morning-819362_640.jpg',
    title: 'Morning',
    author: 'fancycrave1',
  },
  {
    img: 'images/grid-list/hats-829509_640.jpg',
    title: 'Hats',
    author: 'Hans',
  },
  {
    img: 'images/grid-list/honey-823614_640.jpg',
    title: 'Honey',
    author: 'fancycravel',
  },
  {
    img: 'images/grid-list/vegetables-790022_640.jpg',
    title: 'Vegetables',
    author: 'jill111',
  },
  {
    img: 'images/grid-list/water-plant-821293_640.jpg',
    title: 'Water plant',
    author: 'BkrmadtyaKarki',
  },
];

class ListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAdd: false,
      nameAdd: '',
      urlAdd: '',
      priceAdd: ''
    }
  }

  handleOpen() {
    this.setState({
      openAdd: true,
    })
  }

  clearInputs() {
    this.setState({
      nameAdd: '',
      urlAdd: '',
      priceAdd: ''
    })
  }


  handleClose() {
    this.setState({
      openAdd: false
    });
    this.clearInputs.bind(this);
  }

  validateInput(){
    return true;
  }

  handleSubmit() {
    this.validateInput();
    console.log("Submit item from component", this.props);
    this.props.dispatch(ActionCreator.registerItemToList({name:this.state.nameAdd, price:this.state.priceAdd, url:this.state.urlAdd, index:this.props.current_list_index}));
    this.handleClose.bind(this);
  }

  handleTap(id){
    console.log("Tap", this.props.current_list_object, this.props.list_elements);
    this.props.dispatch(ActionCreator.enterToListItem({id}))
    this.props.history.push('/home/lists/list:'+this.props.current_list_id + '/'+'product'+id);
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose.bind(this)}
      />,
      <FlatButton
        label="Ok"
        primary={true}
        onTouchTap={this.handleSubmit.bind(this)}
      >
      </FlatButton>
    ];
    return (
      <div style={styles.root}>
        <h1>List Item</h1>
        <Dialog
          title="Add a friend to your list"
          open={this.state.openAdd}
          onRequestClose={this.handleClose.bind(this)}
          actions={actions}
          modal={false}
        >
          Enter the name of the product
          <TextField hintText="Title"

            //className="login-item"
                     value={this.state.nameAdd}
                     onChange={(e) => {
                       this.setState({
                         nameAdd: e.target.value
                       })
                     }}
          /><br/>
          Enter the price of the product
          <TextField hintText="Title"

            //className="login-item"
                     value={this.state.priceAdd}
                     onChange={(e) => {
                       this.setState({
                         priceAdd: e.target.value
                       })
                     }}
          /><br/>
          Enter the url of the product
          <TextField hintText="Title"

            //className="login-item"
                     value={this.state.urlAdd}
                     onChange={(e) => {
                       this.setState({
                         urlAdd: e.target.value
                       })
                     }}
          />
        </Dialog>
                <GridList
          cellHeight={180}
          style={styles.gridList}
        >
          {this.props.current_list_object.elements.map((tile) => (
            <GridTile
              key={tile.id}
              title={tile.name}
              //actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
              onTouchTap={() => this.handleTap(tile.id)}
            >
              {/*<img src={tile.img} />*/}
              <SocialPerson className="gift-icon"/>
            </GridTile>
          ))}
        </GridList>
          <RaisedButton label="Add"
                      className=""
                      onTouchTap={this.handleOpen.bind(this)}
        />
        <RaisedButton label="Invite friend"
                      className=""
                      onTouchTap={this.handleOpen.bind(this)}
        />
        <RaisedButton label="Back"
                      className=""
                      onTouchTap={() => this.props.history.goBack()}
        />
      </div>
    )
  }
}
export default connect((state) => {
  return {
    current_list_id: state.session.active_list_id,
    current_user_id: state.session.active_user_id,
    current_list_object: state.lists.find((list) => list.id === state.session.active_list_id),
    current_list_index: state.lists.map((list) => list.id).indexOf(state.session.active_list_id),
    list_elements: state.lists.filter((list) => list.id === state.session.active_list_id).map((list) => list.elements)
  }
  }
)(ListItem)