/**
 * Created by sergio on 4/20/17.
 */
import React, {Component} from  'react';
import {GridList, GridTile} from 'material-ui/GridList';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import IconButton from 'material-ui/IconButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import ActionGiftCard from 'material-ui/svg-icons/action/card-giftcard'
import ContentAdd  from 'material-ui/svg-icons/content/add';
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back'
const actionGiftCard = <ActionGiftCard/>;

import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import {connect} from 'react-redux';
import ActionCreator from '../Actions/ActionCreator';

class Lists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAdd: false,
      titleAdd: ""
    }
    this.getOwnerNameByListId = this.getOwnerNameByListId.bind(this);
  }

  handleOpen() {
    this.setState({
      openAdd: true,
    })
  }

  handleClose() {
    this.setState({
      openAdd: false
    })
  }

  handleSubmit() {
    console.log("Submit list from lists")
    if (this.state.titleAdd.length >= 1) {
      console.log(this.state)
      this.props.dispatch(ActionCreator.registerList({name: this.state.titleAdd, owner_id: this.props.owner_id}));
      this.handleClose.bind(this);
      this.props.history.push('lists/list:' + this.state.titleAdd)
      this.setState({titleAdd: ""});
    }
  }

  findNameByList(list_id) {
    const list = this.props.lists.find((aList) => {
      return aList.id === list_id;
    })
    const user = this.props.list.find((user) => {
      return user.id === list.owner_id;
    })
    console.log(user.name);
  }

  handleTap(id) {
    this.props.dispatch(ActionCreator.enterList({id}))
    this.props.history.push('/home/lists/list:' + id);
  }

  componentDidMount() {
    console.log("Lists", this.props)
  }

  getOwnerNameByListId(id){
    console.log("Lists", "by id", id, this.props);
    const res = this.props.users.find((user) => {
      return user.lists_id.includes(id);
    })
    return res ? res.name : "No owner";
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose.bind(this)}
      />,
      <FlatButton
        label="Ok"
        primary={true}
        onTouchTap={this.handleSubmit.bind(this)}
      >
      </FlatButton>
    ];
    return (
      <div className="list-main-wrapper">
        <div className="list-wrapper">
          <Dialog
            title="Create new wishlist"
            open={this.state.openAdd}
            onRequestClose={this.handleClose.bind(this)}
            actions={actions}
            modal={false}
          >
            Input the title for the new wishlist
            <TextField hintText="Title"
              //className="login-item"
                       value={this.state.titleAdd}
                       onChange={(e) => {
                         this.setState({
                           titleAdd: e.target.value
                         })
                       }}
            />
          </Dialog>
          <GridList
            cellHeight={180}
            className="list"
          >
            {this.props.lists.map((tile) => (
              <GridTile
                key={tile.id}
                title={tile.name}
                subtitle={<span>by <b>{this.getOwnerNameByListId(tile.id)}</b></span>}
                actionIcon={<IconButton><StarBorder color="white"/></IconButton>}
                onTouchTap={() => this.handleTap(tile.id)}
              >
                {/*<img src={tile.img} />*/}
                <ActionGiftCard className="gift-icon"/>
              </GridTile>
            ))}
          </GridList>
        </div>
        <div className="list-buttons">
            <FloatingActionButton
              className=""
              onTouchTap={() => this.props.history.goBack()}
            >
              <NavigationArrowBack/>
            </FloatingActionButton>
            <FloatingActionButton
              className=""
              onTouchTap={this.handleOpen.bind(this)}
            >
              <ContentAdd/>
            </FloatingActionButton>
          </div>
      </div>
    )
  }
}
export default connect((state) => {
  return {
    users: state.users.db,
    owner_id: state.session.active_user_id,
    lists: state.lists,
    current_user_index: state.users.db.map((user) => {
      return user.id
    }).indexOf(state.session.active_user_id),
    current_user_object: state.users.db.find((user) => {
      return user.id === state.session.active_user_id
    })
  }
})(Lists)