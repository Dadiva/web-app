/**
 * Created by sergio on 4/20/17.
 */
import React, { Component } from  'react';
import { Router, Route, Redirect, HashRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactGA from 'react-ga'
import createHistory from 'history/createBrowserHistory'

import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
  brown200,
  brown50,
  brown100,
  brown300,
  brown400,
  brown500,
  brown600,
  white,
  darkBlack,
  fullBlack
} from 'material-ui/styles/colors';

import './Styles/styles.css';


import LoginComponent from './login-component';
import Register from './Register';
import Home from './Home';
import Lists from './Lists';
import Friends from './Friends';
import ListItem from './ListItem';
import FriendItem from './FriendItem';
import BottomNav from './BottomNav';
import Product from './Product';
import TopNav from './TopBar';

const customTheme = getMuiTheme({
  palette: {
    primary1Color: brown500,
    primary2Color: brown100,
    primary3Color: brown200,
    accent1Color: brown400,
    accent2Color: brown100,
    accent3Color: brown500,
    textColor: brown600,
    alternateTextColor: white,
    canvasColor: brown50
  }
});

ReactGA.initialize('UA-98350741-1');
const history = createHistory();
history.listen((location, action) => {
  ReactGA.set({ page: location.pathname });
  ReactGA.pageview(location.pathname);
});
class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggged_in: true
    };
  }

  MyTopNav = (props) => {
    return <TopNav />;
  };

  render() {
    const redirect = () => {
      console.log('Redirect', this.props);
      if (!this.props.active_user_id) {
        console.log('land', this.props);
        return <Redirect to="/login"/>;
      } else if (this.props.location.pathname === '/') {
        // return <Redirect to="/home/lists"/>
      } else if (this.props.location.pathname === '/home') {
        //return <Redirect to="/home/lists"/>
      }
    };
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(customTheme)}>
        <Router history={history}>
          <div className="main-wrapper">
            <Route path="/home" component={TopNav}/>
            <Route path="/home" component={Home}/>
            <div className="main-wrapper2">
              {redirect()}
              <Route path="/login" component={LoginComponent}/>
              <Route path="/register" component={Register}/>
              <Route exact path="/home/lists" component={Lists}/>
              <Route exact path="/home/friends" component={Friends}/>
              <Route exact path="/home/lists/list:id" component={ListItem}/>
              <Route exact path="/home/lists/list:id/product:id" component={Product}/>
              {/*Temp*/}
              <Route exact path="/home/friends/friend:id" component={FriendItem}/>
              {/*Temp*/}
            </div>
            <Route path="/home" component={BottomNav}/>
          </div>
        </Router>
      </MuiThemeProvider>
    );
  }
}
export default connect((state) => {
  return {
    active_user_id: state.session.active_user_id
  };
})(Main);
