/**
 * Created by sergio on 4/22/17.
 */
import React, {Component} from  'react';
import {connect} from 'react-redux';
class Product extends Component{
  render(){
    console.log("Props",this.props);
    console.log("Elements",this.props.list.elements.find((element) => element.id === this.props.active_list_item_id));
    return (
      <div>
        <h3>{this.props.product.name}</h3>
        <p>{this.props.product.price}</p>
        <p>{this.props.product.url}</p>
      </div>
    )
  }
}
export default connect(
  (state) => {
    return {
      "active_list_item_id": state.session.active_list_item_id,
      list: state.lists.find((list) => list.id === state.session.active_list_id),
      product: state.lists
        .find((list) => list.id === state.session.active_list_id).elements
        .find((element) => element.id === state.session.active_list_item_id),
    }
  }
)(Product);