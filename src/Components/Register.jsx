/**
 * Created by sergio on 4/20/17.
 * TODO: Refactor errorText
 */
import React, {Component} from  'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import ActionCreator from '../Actions/ActionCreator';
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      last_name: '',
      email: '',
      password: '',
      confirmPassword: '',
      nameError:'',
      lastNameError:'',
      emailError:'',
      passwordError:'',
      confirmPasswordError:'',
    }
    this.handleRegister = this.handleRegister.bind(this);
    this.validatePassword = this.validatePassword.bind(this);
  }

  handleRegister() {
    const {dispatch} = this.props;
    console.log("Register fired");
    if (this.validatePassword()) {
      const person = {
        name: this.state.name,
        last_name: this.state.last_name,
        email: this.state.email,
        password: this.state.password,
      };
      dispatch(ActionCreator.register(person));
      this.props.history.push('/home');
    }

  }

  validatePassword() {
    let res = true;
    const errorMessage = "This field is required";
    if (this.state.name === "") {
      this.setState({nameError: errorMessage});
      res = false;
    } else if (this.state.name !== ""){
      res = res && true;
      this.setState({nameError: ""});
    }
    if (this.state.last_name === "") {
      this.setState({lastNameError: errorMessage});
      res = false;
    } else if (this.state.last_name !== ""){
      res = res && true;
      this.setState({lastNameError: ""});
    }
    if (this.state.email === "") {
      this.setState({emailError: errorMessage});
      res = false;
    } else if (this.state.email !== ""){
      res = res && true;
      this.setState({emailError: ""});
    }
    if (this.state.password === "") {
      this.setState({passwordError: errorMessage});
      res = false;
    } else if (this.state.password !== ""){
      res = res && true;
      this.setState({passwordError: ""});
    }
    if (this.state.confirmPassword === "") {
      this.setState({confirmPasswordError: errorMessage});
      res = false;
    } else if (this.state.confirmPassword !== ""){
      res = res && true;
      this.setState({confirmPasswordError: ""});
    }
    if (this.state.confirmPassword !== this.state.password) {
      this.setState({confirmPasswordError: "Passwords do not match"});
      this.setState({confirmPassword:""})
      this.setState({password:""})
      res = false;
    } else if (this.state.confirmPassword === this.state.password){
      res = res && true;
      this.setState({confirmPasswordError: ""});
    }

    //
    //   && this.state.last_name !== "" && this.state.email !== "" && this.state.name !== "") {
    //   return true;
    // }
    //
    // return res;
    return res;
  }

  render() {
    return (
      <div className="wrapper">
        <Paper className="login">
          <Paper zDepth={0} className="paper">
            <TextField hintText="Name"
              //floatingLabelText="Name"
                       className="login-item"
                       value={this.state.name}
                       errorText={this.state.nameError}
                       onChange={(e) => {
                         this.setState({
                           name: e.target.value
                         })
                       }}
                       onBlur={(e) => {
                         if (e.target.value === '') {
                           console.log('Empty');
                         }
                       }}
            />
            <TextField hintText="Last Name"
              //floatingLabelText="Last Name"
                       className="login-item"
                       value={this.state.last_name}
                       errorText={this.state.lastNameError}
                       onChange={(e) => {
                         this.setState({
                           last_name: e.target.value
                         })
                       }}
            />
            <TextField hintText="Email address"
              //floatingLabelText="Email address"
                       className="login-item"
                       errorText={this.state.emailError}
                       value={this.state.email}
                       onChange={(e) => {
                         this.setState({
                           email: e.target.value
                         })
                       }}
            />
            {/*<TextField hintText="Confirm Email address"
                       className="login-item"
                       onBlur={(e) => {
                         this.setState({
                           alt_email: e.target.value
                         })
                       }}
            />*/}
            <TextField hintText="Password"
                       type="password"
                       errorText={this.state.passwordError}
                       className="login-item"
                       value={this.state.password}
                       onChange={(e) => {
                         this.setState({
                           password: e.target.value
                         })
                       }}
            />
            <TextField hintText="Confirm Password"
                       type="password"
                       className="login-item"
                       value={this.state.confirmPassword}
                       errorText={this.state.confirmPasswordError}
                       onChange={(e) => {
                         this.setState({
                           confirmPassword: e.target.value
                         })
                       }}
                       onBlur={(e) => {
                         console.log('Blur password')
                       }}
            />
            <RaisedButton label="Create Account" className="login-item login-button"
                          onTouchTap={this.handleRegister}/>
          </Paper>
        </Paper>
      </div>
    )
  }
}

export default connect()(Register);