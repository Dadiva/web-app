/**
 * Created by sergio on 4/28/17.
 */
import React, {Component} from  'react';
import {connect} from 'react-redux';

import ActionCreator from '../Actions/ActionCreator'

import IconButton from 'material-ui/IconButton';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import MenuItem from 'material-ui/MenuItem';


import NavigationMenu from 'material-ui/svg-icons/navigation/menu'
import ActionPowerSettingsNew from 'material-ui/svg-icons/action/power-settings-new'
class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDrawer: false,
    }
  }

  handleTap = () => this.setState({openDrawer: !this.state.openDrawer})

  handleClose = () =>{
    this.setState({openDrawer:false})
    console.log("Top bar logging out");
    this.props.dispatch(ActionCreator.logout());
  }


  render() {
    return (
      <div>
        <AppBar
          title="Dādivo"
          iconElementLeft={<IconButton
            onTouchTap={() => this.handleTap()}
          >
            <NavigationMenu/>
          </IconButton>}/>
        <Drawer
          docked={false}
          open={this.state.openDrawer}
          onRequestChange={(openDrawer) => this.setState({openDrawer})}
        >
          <MenuItem onTouchTap={this.handleClose} primaryText="Logout" leftIcon={<ActionPowerSettingsNew/>}/>
        </Drawer>
      </div>
    )
  }
}

export default connect()(TopBar)