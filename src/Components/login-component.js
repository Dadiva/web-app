/**
 * Created by sergio on 4/6/17.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';

import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import {Link} from 'react-router-dom';
import ActionCreator from '../Actions/ActionCreator'


import logo1 from '../../assets/logo1.png';

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    }
  }

  handleRegister() {
    this.props.history.push('/register');
  }

  handleLogin() {
    console.log(this.props.users);
    const res = this.props.users.find((user) => (user.email === this.state.email && user.password === this.state.password)).id
    console.log("LOGIN RES", res);
    if (res.length >= 1) {
      this.props.dispatch(ActionCreator.login({id: res}));
      this.props.history.push('/home/lists');
    }
  }

  render() {
    return (
      <div className="wrapper">
          <Paper zDepth={0} className="login" >
            <div className="logo">
              <img src={logo1} alt=""/>
            </div>
            <Paper zDepth={0} className="paper">
              <TextField
                hintText="Email address"
                floatingLabelText="Email address"
                value={this.state.email}
                className="login-item"
                onChange={(e) => {
                  this.setState({
                    email: e.target.value
                  })
                }}
              />
              <TextField hintText="Password"
                         className="login-item"
                         value={this.state.password}
                         type="password"
                         onChange={(e) => {
                           this.setState({
                             password: e.target.value
                           })
                         }}
              />

              <div className="login-buttons">
              <RaisedButton label="Login"
                            className="login-item login-button"
                            onTouchTap={this.handleLogin.bind(this)}/>
              <RaisedButton label="Register"
                            className="login-item login-button"
                            onTouchTap={this.handleRegister.bind(this)}/>
              </div>

            </Paper>
          </Paper>
      </div>
    )
  }
}

export default connect(
  (state) => {
    return {
      users: state.users.db,
    }
  }
)(LoginComponent);

