/**
 * Created by sergio on 4/20/17.
 */
import React, {Component} from  'react';
import ActionList from '../Actions/ActionList'


export const userReducer = (state = {"db": []}, action) => {
  switch (action.type) {
    case ActionList.ADD_USER:
      console.log("Add user");
      return {
        ...state,
        db: state.db.concat(
          {
            name: action.name,
            last_name: action.last_name,
            id: action.id,
            email: action.email,
            password: action.password,
            friends_id: [],
            lists_id:[]
          }
        )
      };
    case ActionList.ADD_FRIEND:
      //Receives index of owner and id of list
      console.log("Add friend from reducer", action, state);
      const friend_id = state.db.find((user) => {
        return user.email === action.friend_email
        //return user.email === action.friend_email
      }).id;
      /*const owner_index = state.db.map((user) => {
        return user.id
      }).indexOf(action.owner_id);*/
      console.log("The friend is from reducer ", friend_id);
      console.log("My index from reducer", action.owner_index);
      let new_user = {
        ...state.db[action.owner_index],
        friends_id: [
          ...state.db[action.owner_index].friends_id,
          friend_id,
        ]
      };
      console.log(new_user);
      const new_index = action.owner_index++;
      return {
        ...state,
        db: [
          ...state.db.slice(0, action.owner_index-1),
          new_user,
          ...state.db.slice(action.owner_index)
        ]
      };
      //Receives id of owner and id of list
    case ActionList.SET_LIST_OWNER:
      console.log("Set list owner from reducer", action, state);
      const owner_index = state.db.map((user) => {
        return user.id
      }).indexOf(action.owner_id);
      new_user = {
        ...state.db[owner_index],
        lists_id: [
         ...state.db[owner_index].lists_id,
          action.id,
        ]
      }
      console.log("Created new user")
       return {
         ...state,
         db: [
           ...state.db.slice(0, owner_index),
           new_user,
           ...state.db.slice(owner_index + 1)
         ]
       }
    default:
      return state;
  }
};
const defaultSession = {
    "active_user_id": undefined,
    "active_list_id": undefined,
    "active_list_item_id": undefined,
    "active_friend-id":  undefined,
    "active-tab": undefined
}
export const sessionReducer = (state = {...defaultSession}, action) => {
  switch (action.type) {
    case ActionList.LOGIN_USER:
      console.log("Login User")
      return {
        ...state,
        active_user_id: action.id,
        active_tab: 1
      }
    case ActionList.ENTER_TO_LIST:
      console.log("Enter to list");
      return {
        ...state,
        active_list_id: action.id
      }
      return state;

    case ActionList.LOGOUT:
      console.log("logout");
      return { ...defaultSession};

    case ActionList.ENTER_TO_LIST_ITEM:
      console.log("Enter to list item");
      return {
        ...state,
        active_list_item_id: action.id
      }
    default:
      return state
  }
}

export const listReducer = (state = [], action) => {
  switch (action.type) {
    case ActionList.ADD_LIST:
      console.log("Reducer Create list", action);
      return [
        ...state,
        {
          name: action.name,
          owner_id: action.owner_id,
          id: action.id,
          elements: []
        }
      ];
    case ActionList.ADD_ITEM_TO_LIST:
      console.log("Reducer Add item to list", action, "State", state);
      const new_element = {
        name: action.name,
        url: action.url,
        price: action.price,
        id: action.id
      }
      const newArrayEntry = {
        ...state[action.index],
        elements: [
          ...state[action.index].elements,
          new_element
        ]
      }
      let new_state = [
        ...state.slice(0, action.index),
        newArrayEntry,
        ...state.slice(action.index + 1),
      ]
      return new_state;
    default:
      return state;
  }
}

