/**
 * Created by sergio on 4/20/17.
 */
import {combineReducers, createStore, compose, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk'
import {userReducer, listReducer, sessionReducer} from '../Reducers/reducers';
import {autoRehydrate, persistStore} from 'redux-persist';

export const configure = () => {
  const reducer = combineReducers({
    users: userReducer,
    lists: listReducer,
    session: sessionReducer
  })

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(reducer, composeEnhancers(
    applyMiddleware(ReduxThunk),
    //autoRehydrate(),
  ))

  //persistStore(store);
  return store;
}
